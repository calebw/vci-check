function! Vci()
python3 << EOF
import vim, json, os
from urllib import request, parse


data = vim.current.buffer

ci_data = ""
for x in data:
    ci_data+=(x + '\n')

url = 'https://gitlab.com/api/v4/ci/lint'
body = {'content': ci_data}
headers = {'content-type': 'application/json', 'PRIVATE-TOKEN': os.getenv('VCI_TOKEN')}

body = json.dumps(body).encode('utf-8')

# Fetch respon from CI lint API
req = request.Request(url, data=body, headers=headers)
resp_text = request.urlopen(req).read().decode('UTF-8')
json_obj = json.loads(resp_text)

if json_obj['status'] == 'valid':
  vim.command('echohl WarningMsg | echon "Status: "')
  vim.command('echohl Type | echon "valid\n"')

  vim.command('echohl WarningMsg | echon "Warnings: "')
  for warning in json_obj['warnings']:
    vim.command("echohl Statement | echon '{}\n'".format(warning))

else:
  vim.command('echohl WarningMsg | echon "Status: "')
  vim.command('echohl Error | echon "invalid\n"')

  vim.command('echohl WarningMsg | echon "Errors: "')
  for error in json_obj['errors']:
    vim.command('echohl Statement')
    vim.command("echon '{}\n'".format(error))

  vim.command('echohl WarningMsg | echon "Warnings: "')
  for warning in json_obj['warnings']:
    vim.command("echohl Statement | echon '{}'\n".format(warning))


EOF
endfunction
command! -nargs=0 Vci call Vci()

