# Validate Gitlab CI (vci)

Vim plugin for validating `.gitlab-ci.yml` files against GitLab's CI Lint API. 

![Here's an example!](/doc/vci_example.gif "example")

## Prerequisites

- Vim installation with Python 3 support - [How to Check If Vim Supports Python](https://www.feliciano.tech/blog/how-to-check-if-vim-supports-python/).
- [Vundle](https://github.com/VundleVim/Vundle.vim) (Vim plugin manager).
- A [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with a scope of `api` available via an environment variable named `VCI_TOKEN`.

## Installation 

1. Install and configure [Vundle](https://github.com/VundleVim/Vundle.vim) if you haven't already. 

1. Add the following line to your `~/.vimrc`:

    ```
     Plugin 'https://gitlab.com/calebw/vci-check.git'
    ```

1. Install the newly added plugin:

   In a new `vim` session:

   ```
   :PluginInstall!
   ```

## Usage

With CI/CD script open in Vim (e.g. `.gitlab-ci.yml`):

```
:Vci
```
